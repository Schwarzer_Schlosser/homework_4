class Reverseiter:
    def __init__(self, lst):
        self.lst = lst
        self.index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.index - 1 >= -(len(self.lst)):
            self.index = self.index - 1
            return self.lst[self.index]
        else:
            raise StopIteration


reverse_iter = Reverseiter(['q', 'w', 'e', 'r'])

for i in reverse_iter:
    print(i)
