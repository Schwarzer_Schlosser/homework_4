class FormulaError(Exception):
    pass


def calc(data):
    lst = data.split()
    if len(lst) == 3 and lst[1] in "+-*/":
        try:
            num1 = float(lst[0])
            num2 = float(lst[2])
        except ValueError:
            raise FormulaError
        else:
            if lst[1] == "+":
                return num1 + num2
            elif lst[1] == "-":
                return num1 - num2
            elif lst[1] == "*":
                return num1 * num2
            elif lst[1] == "/":
                return num1 / num2
    else:
        raise FormulaError


print(calc("2 + 22"))
