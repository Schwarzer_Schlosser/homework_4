def fib_generator(n):
    first, second = 0, 1
    for _ in range(n):
        yield first
        first, second = second, first + second


def fib_list(n):
    fibs = [0, 1]
    if n == 1:
        return fibs[:n]
    for ind in range(n - 2):
        fibs.append(fibs[ind] + fibs[ind+1])
    return fibs


fibi_gen = fib_generator(6)
for i in fibi_gen:
    print(i, end=' ')

print()

fibi_list = fib_list(5)
print(fibi_list)
